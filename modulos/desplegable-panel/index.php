<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="/public/skin/default/js/parallax.js" ></script>
	<link rel="stylesheet" href="/public/skin/default/css/grid.css">
	<link rel="stylesheet" href="/public/skin/default/css/style.css">
	<title>Desplegable Panel</title>
</head>
<body>
	<div class="w-xxxx-12">
		<div class="content">
			<a href="/" class="btn_back"><</a>
			<h1>Desplegable Panel</h1>
			<div class="cont_panel_more">
				<i class="panel_more">•••</i>
				<ul>
					<li><a href="#">Inactivo</a></li>
					<li><a href="#">Editar</a></li>
					<li><a href="#">Eliminar</a></li>
				</ul>
			</div>
		</div>
	</div>
</body>
</html>