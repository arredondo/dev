<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="/public/skin/default/js/parallax.js" ></script>
	<link rel="stylesheet" href="/public/skin/default/css/grid.css">
	<link rel="stylesheet" href="/public/skin/default/css/style.css">
	<title>Parallax</title>
</head>
<body>
	<a href="/" class="btn_back"><</a>
	<section>
		<div class="header-bg bg_1"></div>
		<div class="section_content">
			<h1>Parallax</h1>
			<h2>Hola</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos, hic explicabo in. Tenetur ipsum, eligendi nam officiis accusamus, hic similique aperiam, esse sed assumenda dignissimos et quod molestias! Cumque rerum illum sapiente incidunt labore voluptates reprehenderit impedit sit, qui, repellendus, ad magni ex nulla accusamus modi dignissimos sequi consequuntur est.</p>
		</div>
	</section>
	<section>
		<div class="header-bg bg_2"></div>
		<div class="section_content">
			<h2>Hola</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos, hic explicabo in. Tenetur ipsum, eligendi nam officiis accusamus, hic similique aperiam, esse sed assumenda dignissimos et quod molestias! Cumque rerum illum sapiente incidunt labore voluptates reprehenderit impedit sit, qui, repellendus, ad magni ex nulla accusamus modi dignissimos sequi consequuntur est.</p>
			<div class="flotante_top">Hola</div>
			<div class="flotante_bottom">Hola</div>
		</div>
	</section>
	<section>
		<div class="header-bg bg_3"></div>
		<div class="section_content">
			<h2>Hola</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos, hic explicabo in. Tenetur ipsum, eligendi nam officiis accusamus, hic similique aperiam, esse sed assumenda dignissimos et quod molestias! Cumque rerum illum sapiente incidunt labore voluptates reprehenderit impedit sit, qui, repellendus, ad magni ex nulla accusamus modi dignissimos sequi consequuntur est.</p>
			<div class="flotante_top">Hola</div>
			<div class="flotante_bottom">Hola</div>
		</div>
	</section>
	<section>
		<div class="header-bg bg_4"></div>
		<div class="section_content">
			<h2>Hola</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos, hic explicabo in. Tenetur ipsum, eligendi nam officiis accusamus, hic similique aperiam, esse sed assumenda dignissimos et quod molestias! Cumque rerum illum sapiente incidunt labore voluptates reprehenderit impedit sit, qui, repellendus, ad magni ex nulla accusamus modi dignissimos sequi consequuntur est.</p>
			<div class="flotante_top">Hola</div>
			<div class="flotante_bottom">Hola</div>
		</div>
	</section>


</body>
</html>