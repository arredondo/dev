$(window).scroll(function() {
	var scrollTop = $(this).scrollTop();
	
	$('div[class*=bg_]').each(function(index,val){

		var vel = parseInt($(this).attr('class').replace(/[^\d]/g, ''));

		vel = (0+(vel * .3 ))/parseInt($('div[class*=bg_]').length);

		$(this).css('top', -(scrollTop * vel) + 'px');

	});

	$('.flotante_top').css('top', -(scrollTop * 0.4) + 'px');
	$('.flotante_bottom').css('top', (scrollTop * 0.1) + 'px');
});