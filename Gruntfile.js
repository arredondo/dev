module.exports = function(grunt) {
  require('jit-grunt')(grunt);

  grunt.initConfig({
    less: {
      development: {
        options: {
          compress: false, // No está en una sola linea, para hacerlo, solo poner true
          yuicompress: true,
          optimization: 2
        },
        files: { // destination file and source file
          "public/skin/default/css/style.css": "public/skin/default/less/style.less", "public/skin/default/css/grid.css": "public/skin/default/less/grid.less"
        }
      }
    },
    watch: {
      less: {
            files: ['public/skin/default/less/*.less'],
            tasks: ['less'],
            options: {
                livereload: true
            }
        }
    }
  });

  grunt.registerTask('default', ['less', 'watch']);
};
